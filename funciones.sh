#!/bin/bash

function runserver(){
python manage.py runserver
}

function shell(){
python manage.py shell -i bpython
}

function worker() {
celery -A web worker -E -l info
}

function beat() {
celery -A web beat -l debug -S django
}

function dj_flower(){
celery -A web flower --port=5555
}

function dj_inspect(){
celery -A web inspect registered
}
