# Create your tasks here
from __future__ import absolute_import, unicode_literals

from celery import shared_task


@shared_task
def sumar(*args):
    return sum(args)

@shared_task
def dividir(a,b):
    if b!=0:
        return a/b
    else:
        return None

@shared_task
def capitalize(palabra):
    return palabra.capitalize()
